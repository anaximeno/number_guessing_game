#!/bin/bash
INF_LIMIT=1
SUP_LIMIT=1000
SECRET_NUMBER=$(( $INF_LIMIT + RANDOM % $SUP_LIMIT ))
NUMBER_OF_GUESSES=0

PSQL="psql --username=freecodecamp --dbname=number_guess -t --no-align -c"

GUESS_GAME() {
  NUMBER_OF_GUESSES=$(( NUMBER_OF_GUESSES + 1 ))

  if [[ -n $1 ]]
  then
    echo -e "\n$1"
  else
    echo -e "\nGuess the secret number between $INF_LIMIT and $SUP_LIMIT:"
  fi

  read USER_INPUT

  if [[ ! $USER_INPUT =~ ^[0-9]+$ ]]
  then
    # if input is not numeric
    GUESS_GAME "That is not an integer, guess again:"
  else
    if [[ $USER_INPUT -gt $SECRET_NUMBER ]]
    then
      GUESS_GAME "It's lower than that, guess again:"
    elif [[ $USER_INPUT -lt $SECRET_NUMBER ]]
    then
      GUESS_GAME "It's higher than that, guess again:"
    else
      GAME_RESULT_INSERT=$($PSQL "INSERT INTO games (number_of_guesses, secret_number, user_id) VALUES ($NUMBER_OF_GUESSES, $SECRET_NUMBER, $USER_ID)")
      echo -e "\nYou guessed it in $NUMBER_OF_GUESSES tries. The secret number was $SECRET_NUMBER. Nice job!"
    fi
  fi
}

echo Enter your username:
read USERNAME

# get user id
USER_ID=$($PSQL "SELECT user_id FROM users WHERE username = '$USERNAME'")

if [[ -z $USER_ID ]]
then
  # if not found
  USER_INSERT_RESULT=$($PSQL "INSERT INTO users(username) VALUES ('$USERNAME')")
  USER_ID=$($PSQL "SELECT user_id FROM users WHERE username = '$USERNAME'")
  echo -e "\nWelcome, $USERNAME! It looks like this is your first time here."
else
  # get the number of games played
  GAMES_PLAYED=$($PSQL "SELECT COUNT(*) FROM users INNER JOIN games USING (user_id) WHERE username = '$USERNAME'")
  # best game guesses
  BEST_GAME=$($PSQL "SELECT MIN(number_of_guesses) FROM users INNER JOIN games USING (user_id) WHERE username = '$USERNAME'")
  echo -e "\nWelcome back, $USERNAME! You have played $GAMES_PLAYED games, and your best game took $BEST_GAME guesses."
fi

GUESS_GAME